<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('users')->group (function()
{
    Route::get('/','UserController@index')->name('users.index');
    Route::get('//create','UserController@create')->name('users.create');
    Route::get('//{id}/edit','UserController@edit')->name('users.edit');
    Route::get('//{id}','UserController@show')->name('users.show');
    Route::post('/','UserController@store')->name('users.store');
    Route::put('//{id}','UserController@update')->name('users.update');
    Route::delete('//{id}','UserController@destroy')->name('users.destroy');
});

Route::prefix('categorys')->group (function()
{
    Route::get('/','CategoryController@index')->name('categorys.index');
    Route::get('//create','CategoryController@create')->name('categorys.create');
    Route::get('//{id}/edit','CategoryController@edit')->name('categorys.edit');
    Route::get('//{id}','CategoryController@show')->name('categorys.show');
    Route::post('/','CategoryController@store')->name('categorys.store');
    Route::put('//{id}','CategoryController@update')->name('categorys.update');
    Route::delete('//{id}','CategoryController@destroy')->name('categorys.destroy');
});

Route::prefix('books')->group (function()
{
    Route::get('/','BukuController@index')->name('books.index');
    Route::get('//create','BukuController@create')->name('books.create');
    Route::get('//{id}/edit','BukuController@edit')->name('books.edit');
    Route::get('//{id}','BukuController@show')->name('books.show');
    Route::post('/','BukuController@store')->name('books.store');
    Route::put('//{id}','BukuController@update')->name('books.update');
    Route::delete('//{id}','BukuController@destroy')->name('books.destroy');
});

Route::prefix('orders')->group (function()
{
    Route::get('/','PesananController@index')->name('orders.index');
    Route::get('//create','PesananController@create')->name('orders.create');
    Route::get('//{id}/edit','PesananController@edit')->name('orders.edit');
    Route::get('//{id}','PesananController@show')->name('orders.show');
    Route::post('/','PesananController@store')->name('orders.store');
    Route::put('//{id}','PesananController@update')->name('orders.update');
    Route::delete('//{id}','PesananController@destroy')->name('orders.destroy');
});
