-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 11 Apr 2019 pada 15.40
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `submission`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `books`
--

CREATE TABLE `books` (
  `id` int(10) UNSIGNED NOT NULL,
  `Judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Penulis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Penerbit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Harga` double(8,2) NOT NULL,
  `deskripsi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Stok` int(11) NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `books`
--

INSERT INTO `books` (`id`, `Judul`, `Penulis`, `Penerbit`, `Harga`, `deskripsi`, `Stok`, `avatar`, `created_at`, `updated_at`) VALUES
(1, 'Hello To My Ex', 'Pit Sansi', 'Loveable', 89500.00, 'Bagaimana mungkin seseorang memiliki\r\nkeinginan untuk mengurai kembali benang yang tak terkirakan jumlahnya dalam selembar sapu tangan yang telah ditenunnya sendiri.', 50, 'avatars/DGSIY9CvFvhvoBbdobnxZ77eqXZYJ6qqyFGvc2id.jpeg', '2019-04-11 04:37:34', '2019-04-11 05:06:15'),
(2, 'Buku Caci Maki', 'LOTTA SONNINEN', 'Gramedia Pustaka Utama', 49000.00, 'Hanya kehidupan sehari-hari tentang Kana, cewek super pemalas yang tidak mau melakukan semua hal karena dianggap repot.', 35, 'avatars/HntQqMbwRPqMxJLt6zTcuJA39sEV0H0SdxkEm1ra.jpeg', '2019-04-11 04:39:35', '2019-04-11 05:05:44'),
(3, 'Mengenal Transportasi dan Profesi', 'Dwi Putra', 'Bhuana Ilmu Populer', 53000.00, 'Buku aktivitas untuk memperkenalkan berbagai alat transportasi dan profesi kepada anak-anak usia dini.', 45, 'avatars/Te2VUiCwXyfySPr9U29ylkCrJgk8Jun4jZONBf4t.jpeg', '2019-04-11 04:41:36', '2019-04-11 04:41:36'),
(4, 'Pingkan Melipat Jarak', 'Sapardi Djoko Damono', 'Gramedia Pustaka Utama', 60000.00, '\"Selalu ada saat ketika kita tidak sempat bertanya kepada sepasang kaki sendiri kenapa tidak mau berhenti sejak mengawali pengembaraan', 20, 'avatars/ada30TiXV6qOoPwFCu04teQTvoA6x1WxWn4G6TUf.jpeg', '2019-04-11 04:43:25', '2019-04-11 04:43:25'),
(5, 'Aruna dan Lidahnya', 'Laksmi Pamuntjak', 'Gramedia Pustaka Utama', 78000.00, 'Ketika Aruna ditugasi meneyelidiki kasus flu unggas yang terjadi secara serentak di delapan kota seputar Indonesia', 10, 'avatars/LdYCYOsgLvm0CB28fy0kFGRsyLzCZi0t6DL7wbNM.jpeg', '2019-04-11 04:52:54', '2019-04-11 04:52:54'),
(6, 'HI-FI', 'Bayu Permana', 'm&c!', 88000.00, 'Terjadi pembunuhan mengerikan terhadap seorang anak laki-laki di kota tempat Honami tinggal. Korban bahkan diperkosa setelah dibunuh.', 40, 'avatars/T25pz0olWUEd1W5JSw4x8CeI0JnmEBaK6x2VYPrD.jpeg', '2019-04-11 04:54:56', '2019-04-11 04:54:56'),
(7, 'Diary Gamophobia', 'Liana Safitri', 'Laksana', 65000.00, 'Aku tidak tahu kenapa kamu selalu melukis orang secara terpisah. Ini\r\nmenyedihkan!', 10, 'avatars/g4quxG4P2C6B278TUj453Us7oCB0LOgxlIwO6pjP.jpeg', '2019-04-11 04:56:05', '2019-04-11 04:56:05'),
(8, 'Aru Shah And The End Of Time', 'Roshani Chokshi', 'Hachette Book', 140000.00, 'Best-selling author Rick Riordan introduces this adventure by Roshani Chokshi about twelve-year-old Aru Shah, who has a tendency to stretch the truth in order to fit in at school.', 30, 'avatars/qYUNC67sMTtZ5q2Xiyi10c2jIqgvl19Nwj8YCd6X.jpeg', '2019-04-11 04:57:09', '2019-04-11 04:57:09'),
(9, 'Untung Ada Dinda Untung Ada Kanda', 'Bret Pitt,  NIkol Kidman', 'Kubus Media', 90000.00, '\"Air mata istri tuh keramat gaes. Dia bisa membuat kita sengsara, bisa juga membuat kita bahagia. Tinggal kitanya mau pilih yang mana. Semoga lelah istri kita berbuah surga.\"', 45, 'avatars/VR5ZCaSbUxvcRkSIT3fGiWrAzqAdBtO68eg0MIsA.jpeg', '2019-04-11 04:58:11', '2019-04-11 04:58:11'),
(10, 'Gatal Menawar', 'Kembangmanggis', 'Gramedia Pustaka Utama', 115000.00, 'Sketsa-sketsa-3 Kembangmanggis “Gatal Menawar” berisi 20 kisah ringan yang terungkap lewat kata-kata sederhana & mudah dicerna.', 30, 'avatars/w9wx59Pv054Lt1zQSZhtNQ2IgiCpCpXb46bqCtDp.jpeg', '2019-04-11 04:59:11', '2019-04-11 04:59:11'),
(11, 'Jangan Sisakan Nasi dalam Piring', 'Kembangmanggis', 'Gramedia Pustaka Utama', 110000.00, 'Sketsa-sketsa-4 Kembangmanggis “Jangan Sisakan Nasi dalam Piring” berisi 23 kisah ringan tentang Ubud, Bali.', 15, 'avatars/cGB5FkFu4nPqGzKzfsZBPbzTkWXG5WefYUnvp7O0.jpeg', '2019-04-11 05:00:12', '2019-04-11 05:00:12'),
(12, 'Musuh Tapi Menikah', 'Naya A.', 'Aksara Plus', 95000.00, 'Nam Jaemin dan Jung Chaehyun bermusuhan sejak SMA. Namun, seperti kisah klise lainnya tentang musuh yang saling jatuh cinta, mereka pun pacaran.', 25, 'avatars/yn0CucWTrVFioSPkNxIPx2OHH4FJ5NnENg4Rc1oj.jpeg', '2019-04-11 05:01:16', '2019-04-11 05:01:16'),
(13, 'Protect the Waterfall Village', 'Masashi Kishimoto', 'Elex Media Komputindo', 55000.00, 'Di perjalanan pulang setelah mengawal Shibuki, kepala desa muda dari Desa Takigakure,', 10, 'avatars/bvb0S5XY2qMpI6bqlroUybBhjAq3gH1Xe83h7DB9.jpeg', '2019-04-11 05:02:49', '2019-04-11 05:02:49'),
(14, 'The Last Piece Of Puzzle', 'Nicko Zainnanda', 'Pastel Books', 70000.00, 'amal dan Zahra telah bersahabat sejak di SD. Diam-diam, ternyata Jamal menyimpan perasaan lain kepada sahabatnya itu.', 60, 'avatars/0pxkGdpJ3By5bWrhWMb4faBuvkvcWI7rcmdqueVf.jpeg', '2019-04-11 05:04:19', '2019-04-11 05:04:19'),
(15, 'Friendshit', 'Queen Nakey', 'RANS Publisher', 72500.00, 'Hanya kehidupan sehari-hari tentang Kana, cewek super pemalas yang tidak mau melakukan semua hal karena dianggap repot.', 45, 'avatars/dQYyy2Jpy4VxlPhAYCX4z4hdSkBsuHVA15dm8l8o.jpeg', '2019-04-11 05:05:20', '2019-04-11 05:05:20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `book_category`
--

CREATE TABLE `book_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `book_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `book_category`
--

INSERT INTO `book_category` (`id`, `book_id`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 2, NULL, NULL),
(3, 3, 3, NULL, NULL),
(4, 4, 4, NULL, NULL),
(5, 5, 6, NULL, NULL),
(6, 6, 1, NULL, NULL),
(7, 7, 7, NULL, NULL),
(8, 8, 13, NULL, NULL),
(9, 9, 14, NULL, NULL),
(10, 10, 7, NULL, NULL),
(11, 11, 12, NULL, NULL),
(12, 12, 11, NULL, NULL),
(13, 13, 10, NULL, NULL),
(14, 14, 14, NULL, NULL),
(15, 15, 15, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `Nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `categories`
--

INSERT INTO `categories` (`id`, `Nama`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, 'Drama', 'Bagus', NULL, NULL),
(2, 'Family', 'Sangat Bagus', NULL, NULL),
(3, 'Horor', 'Lumayan Lah', NULL, NULL),
(4, 'Romance', 'Jelek', NULL, NULL),
(5, 'Action', 'Kurang Baik', NULL, NULL),
(6, 'Adventure', 'Bagus', NULL, NULL),
(7, 'Comedy', 'Bagus Aja', NULL, NULL),
(8, 'Kriminal', 'Lumayan Lah', NULL, NULL),
(9, 'Fantasi', 'Kurang Menarik', NULL, NULL),
(10, 'Musikal', 'Bagus Banget', NULL, NULL),
(11, 'Epik', 'Luamayan Jelek', NULL, NULL),
(12, 'Fiksi Ilmiah', 'Bagus', NULL, NULL),
(13, 'Jagal', 'Bagus', NULL, NULL),
(14, 'Seru', 'Jelek', NULL, NULL),
(15, 'Cerita', 'Jelek', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_04_10_174423_create_books_table', 1),
(4, '2019_04_10_174631_create_categorys_table', 1),
(5, '2019_04_10_174757_create_orders_table', 1),
(6, '2019_04_11_024517_create_book_category_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `Nomor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Submit','Process','Finish','Cancel') COLLATE utf8mb4_unicode_ci NOT NULL,
  `Total` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `Nomor`, `status`, `Total`, `created_at`, `updated_at`) VALUES
(1, 1, '1715015091', 'Finish', '15000', '2019-04-11 05:06:56', '2019-04-11 05:08:54'),
(2, 2, '1715015092', 'Process', '20000', '2019-04-11 05:07:26', '2019-04-11 05:09:03'),
(3, 3, '1715015093', 'Cancel', '25000', '2019-04-11 05:07:46', '2019-04-11 05:09:13'),
(4, 4, '1715015094', 'Submit', '30000', '2019-04-11 05:08:13', '2019-04-11 05:09:20'),
(5, 5, '1715015095', 'Process', '15000', '2019-04-11 05:08:43', '2019-04-11 05:09:27'),
(6, 6, '1715015096', 'Process', '40000', '2019-04-11 05:10:12', '2019-04-11 05:10:12'),
(7, 7, '1715015097', 'Cancel', '35000', '2019-04-11 05:10:42', '2019-04-11 05:10:42'),
(8, 8, '1715015098', 'Cancel', '70000', '2019-04-11 05:11:03', '2019-04-11 05:11:03'),
(9, 9, '1715015099', 'Submit', '35000', '2019-04-11 05:11:40', '2019-04-11 05:11:40'),
(10, 10, '1715015100', 'Process', '35000', '2019-04-11 05:12:03', '2019-04-11 05:12:03'),
(11, 11, '1715015101', 'Finish', '25000', '2019-04-11 05:12:28', '2019-04-11 05:12:28'),
(12, 12, '1715015102', 'Cancel', '55000', '2019-04-11 05:12:49', '2019-04-11 05:12:49'),
(14, 13, '1715015103', 'Process', '25000', '2019-04-11 05:13:53', '2019-04-11 05:13:53'),
(15, 14, '1715015104', 'Submit', '10000', '2019-04-11 05:14:18', '2019-04-11 05:14:18'),
(16, 15, '17150150118', 'Cancel', '100000', '2019-04-11 05:14:41', '2019-04-11 05:14:41');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `Nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `Phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `Nama`, `Email`, `email_verified_at`, `Phone`, `Address`, `avatar`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Dandy Indra Wardhana', 'dandyndr@gmail.com', NULL, '081352256796', 'Jl.pramuka 12', 'avatars/aYJ4dzfieAV7N0NZ4FUjBgoRbobQvbHyrDYAdykV.jpeg', '$2y$10$ESZfRo4xYRgusR8XiBds2OAXb.XpiJrK464NUNGv8fLZB8YHV3u52', NULL, NULL, '2019-04-11 05:15:46'),
(2, 'Hareray Paradongan P', 'ray27@gmail.com', NULL, '085346298754', 'Jl.perjuangan 4', 'avatars/Fapif9Ga9lKD1rnepNWQ1UdTg5cjoQi3VJOu5b6K.jpeg', '$2y$10$0eG2jiWolSjx/M8wEGzk/.8qTg1kpXNI75T4YqSt0JoY6aNsRGmai', NULL, NULL, '2019-04-11 05:15:57'),
(3, 'Abdul Muiz Arizzqi', 'Muizganteng@gmail.com', NULL, '085213958746', 'Jl.bhayangkara no 21', 'avatars/ppOPtfR0OHGa6z7fM5IFPYVGYHKWbCdsfJ43Q0R3.jpeg', '$2y$10$qohFouO/H5qcoRy/GORHAemlSD82.11O///AfV2WGmeVk/hEr2cOu', NULL, NULL, '2019-04-11 05:16:10'),
(4, 'Lily Kurnia Sari', 'oshbae@gmail.com', NULL, '081298576445', 'Jl.suryanata no 98', 'avatars/on794JEepfqXTHHmrQwkWYF7OdQW6X0nlabMmp5R.jpeg', '$2y$10$X4poq0sFEE/JT58zbgWcQu5IfHo9AzlUh4Cq9DdQwfdGQeNlKEYaa', NULL, NULL, '2019-04-11 05:16:19'),
(5, 'Sinthya Ayu Pratiwi', 'sinthyaayu@gmail.com', NULL, '085664733834', 'Jl.bunga mawar no 66', 'avatars/M6i9JGg5k6xU6Tdzk7v8mq9ix5mKVQIQPfeZZdOS.jpeg', '$2y$10$4.XxgUraInfOskb0lrA7tu08Le/Vmo3FednWqM4VrdbG3YafK.j5.', NULL, NULL, '2019-04-11 05:16:30'),
(6, 'Fahmi Adrian', 'Fahmiwibu@gmail.com', NULL, '085346576842', 'Jl.Sanggata no 12', 'avatars/c3esAcsw0WoYn1ffYVBf4STSiVxv3ZbfeecKoi3q.jpeg', '$2y$10$EX0YzjxbLm26JsYkrn.OVOBIDzGS0SVogze7qNNs8W6rC586pZzCu', NULL, NULL, '2019-04-11 05:16:45'),
(7, 'Fahmi Al Furqan', 'leleng76@gmail.com', NULL, '081234567893', 'Jl.Bhayangkara', 'avatars/oVtrQBjPj6eOyo6gkUdl7cZFhKAB5O7DXZtN3CM0.jpeg', '$2y$10$GItAO0plrKV6xXIQTms1j.yjnNPK0pecPfp4Ytsy67LT9PU0Dqc1a', NULL, NULL, '2019-04-11 05:16:59'),
(8, 'Garda Setiawan', 'Z1Ace@gmail.com', NULL, '081246572846', 'Jl.kolong jembatan gg 2', 'avatars/Z3bfqcSxfvUaeiMLp8AhQtUtaXcIBEspAUAHT5SZ.jpeg', '$2y$10$iJm13iWgRFymGrI9ZRpWa.luTkSLFe7psoZDIuQlEe5Fqp0EjVbgO', NULL, NULL, '2019-04-11 05:17:23'),
(9, 'Adnan Fauzan', 'dilan1999@gmail.com', NULL, '085394572846', 'Jl.Suryanata', 'avatars/tuAPDs3RgHGcWBpvTpGOMvIhh5msLAAGmbZ5O6lC.jpeg', '$2y$10$KDo.5Tuw5eUHSggZ5kjrVe/fe0xU3AePTCIvLUy/9GDGio7AF2pBu', NULL, NULL, '2019-04-11 05:17:36'),
(10, 'Ayunda Dwi', 'Yundacantik@gmail.com', NULL, '085278564534', 'Jl.Juanda no 45', 'avatars/DXLxPgUwZKR1krxxYDrP9YV2NE1eJF38zV6wQAq9.jpeg', '$2y$10$yYyaROqDFMtjNj0Kp3427.e4mX.bf2pVYmZiJbHCJYdXtafqJpxnq', NULL, NULL, '2019-04-11 05:17:59'),
(11, 'Rifqi Naufal', 'Naufalrifqi@gmail.com', NULL, '081267564578', 'Jl.Loajanan', 'avatars/LJEOtMUzC3Cu5znskTa71GJxCALQ8pIyou8iustA.jpeg', '$2y$10$BNgbhMuI7lqs5pHHNQ8.9Ojz0XvP0zkEoNWRWocOhVdPX1xwiHgMy', NULL, NULL, '2019-04-11 05:18:22'),
(12, 'Rezha Zahra', 'RezhaNur@gmail.com', NULL, '08994673546', 'Jl.bunga mawar no 98', 'avatars/5gD1kuM9PM5r4dbsKLLGHShxM1vwoAKr6pir61op.jpeg', '$2y$10$3n7mEffj0o4uU9nvVoaeF.Nu8Fn3IhPg1uj86e27bqWDP/5HKrFWe', NULL, NULL, '2019-04-11 05:18:34'),
(13, 'BlackPink', 'killthislove@gmail.com', NULL, '081234567854', 'Jl.Korea Utara', 'avatars/Ts6y1Jeia9DENH1T8afOSP8JDwhUMTxItUaeUCOy.jpeg', '$2y$10$8F.lflrLPXtbHRKfmkQstuhSSY4zWoZPvqQWHXQKKCss2zY4Jicfu', NULL, NULL, '2019-04-11 05:18:47'),
(14, 'Umji', 'Gfriend@gmail.com', NULL, '081234127645', 'Jl.bangau no 26', 'avatars/3KS3F12bmBnuTrpt1vUZb1eUiM4gJ9X21Rzt5hYo.jpeg', '$2y$10$59bHxnDY407dzhMIAEbmzOc5woBLLbbifoLOJFeRmtCZV9pv.Ae3K', NULL, NULL, '2019-04-11 05:18:58'),
(15, 'Aldi Pangestu', 'Adrqra@gmail.com', NULL, '081384566738', 'Jl.kubar barat no 01', 'avatars/K6z2nbLwqCE5h0YLdJqEefXFn0vFzW3n6gTqlO5C.jpeg', '$2y$10$Zkh8Gsmz4bcdAI6rzBkgYOIV3fPoFTYVwKjVb2Q8oiepb2OXmN2Hm', NULL, NULL, '2019-04-11 05:19:11'),
(16, 'laila', 'laila@gmail.com', NULL, '12123123123', 'asdada', 'avatars/hvxvobgyYztlNXasf2cn8K01akpcCdN1x2m3yU1h.jpeg', '$2y$10$mwoandocrpuz5KbkdeGo2u.1hi3yXDvv8Z1U1CscsIjMN71RluRRW', NULL, '2019-04-11 04:17:33', '2019-04-11 04:17:33');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `book_category`
--
ALTER TABLE `book_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `book_category_book_id_foreign` (`book_id`),
  ADD KEY `book_category_category_id_foreign` (`category_id`);

--
-- Indeks untuk tabel `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`Email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `books`
--
ALTER TABLE `books`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `book_category`
--
ALTER TABLE `book_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `book_category`
--
ALTER TABLE `book_category`
  ADD CONSTRAINT `book_category_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`),
  ADD CONSTRAINT `book_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Ketidakleluasaan untuk tabel `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
