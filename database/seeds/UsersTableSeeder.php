<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
          ['id'=>1,
          'Nama' => "Dandy Indra Wardhana",
          'Email' => "dandyndr@gmail.com",
          'Phone' => "081352256796",
          'Address' => "Jl.pramuka 12",
          'avatar'=>'/picture/1.jpg',
          'password'=>Hash::make("123456")
          ],

          ['id'=>2,
          'Nama' => "Hareray Paradongan P",
          'Email' => "ray27@gmail.com",
          'Phone' => "085346298754",
          'Address' => "Jl.perjuangan 4",
          'avatar'=>'/picture/1.jpg',
          'password'=>Hash::make("123456")
          ],

          ['id'=>3,
          'Nama' => "Abdul Muiz Arizzqi",
          'Email' => "Muizganteng@gmail.com",
          'Phone' => "085213958746",
          'Address' => "Jl.bhayangkara no 21",
          'avatar'=>'/picture/1.jpg',
          'password'=>Hash::make("123456")
          ],

          ['id'=>4,
          'Nama' => "Lily Kurnia Sari",
          'Email' => "oshbae@gmail.com",
          'Phone' => "081298576445",
          'Address' => "Jl.suryanata no 98",
          'avatar'=>'/picture/1.jpg',
          'password'=>Hash::make("123456")
          ],

          ['id'=>5,
          'Nama' => "Sinthya Ayu Pratiwi",
          'Email' => "sinthyaayu@gmail.com",
          'Phone' => "085664733834",
          'Address' => "Jl.bunga mawar no 66",
          'avatar'=>'/picture/1.jpg',
          'password'=>Hash::make("123456")
          ],

          ['id'=>6,
          'Nama' => "Fahmi Adrian",
          'Email' => "Fahmiwibu@gmail.com",
          'Phone' => "085346576842",
          'Address' => "Jl.Sanggata no 12",
          'avatar'=>'/picture/1.jpg',
          'password'=>Hash::make("123456")
          ],

          ['id'=>7,
          'Nama' => "Fahmi Al Furqan",
          'Email' => "leleng76@gmail.com",
          'Phone' => "081234567893",
          'Address' => "Jl.Bhayangkara",
          'avatar'=>'/picture/1.jpg',
          'password'=>Hash::make("123456")
          ],

          ['id'=>8,
          'Nama' => "Garda Setiawan",
          'Email' => "Z1Ace@gmail.com",
          'Phone' => "081246572846",
          'Address' => "Jl.kolong jembatan gg 2",
          'avatar'=>'/picture/1.jpg',
          'password'=>Hash::make("123456")
          ],

          ['id'=>9,
          'Nama' => "Adnan Fauzan",
          'Email' => "dilan1999@gmail.com",
          'Phone' => "085394572846",
          'Address' => "Jl.Suryanata",
          'avatar'=>'/picture/1.jpg',
          'password'=>Hash::make("123456")
          ],

          ['id'=>10,
          'Nama' => "Ayunda Dwi",
          'Email' => "Yundacantik@gmail.com",
          'Phone' => "085278564534",
          'Address' => "Jl.Juanda no 45",
          'avatar'=>'/picture/1.jpg',
          'password'=>Hash::make("123456")
          ],

          ['id'=>11,
          'Nama' => "Rifqi Naufal",
          'Email' => "Naufalrifqi@gmail.com",
          'Phone' => "081267564578",
          'Address' => "Jl.Loajanan",
          'avatar'=>'/picture/1.jpg',
          'password'=>Hash::make("123456")
          ],

          ['id'=>12,
          'Nama' => "Rezha Zahra",
          'Email' => "RezhaNur@gmail.com",
          'Phone' => "08994673546",
          'Address' => "Jl.bunga mawar no 98",
          'avatar'=>'/picture/1.jpg',
          'password'=>Hash::make("123456")
          ],

          ['id'=>13,
          'Nama' => "BlackPink",
          'Email' => "killthislove@gmail.com",
          'Phone' => "081234567854",
          'Address' => "Jl.Korea Utara",
          'avatar'=>'/picture/1.jpg',
          'password'=>Hash::make("123456")
          ],

          ['id'=>14,
          'Nama' => "Umji",
          'Email' => "Gfriend@gmail.com",
          'Phone' => "081234127645",
          'Address' => "Jl.bangau no 26",
          'avatar'=>'/picture/1.jpg',
          'password'=>Hash::make("123456")
          ],

          ['id'=>15,
          'Nama' => "Aldi Pangestu",
          'Email' => "Adrqra@gmail.com",
          'Phone' => "081384566738",
          'Address' => "Jl.kubar barat no 01",
          'avatar'=>'/picture/1.jpg',
          'password'=>Hash::make("123456")
          ],

        ]);
    }
}
