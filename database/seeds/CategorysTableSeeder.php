<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('categories')->insert([
            ['id'=>1,'Nama' => "Drama",'deskripsi' => "Bagus"],
            ['id'=>2,'Nama' => "Family",'deskripsi' => "Sangat Bagus"],
            ['id'=>3,'Nama' => "Horor",'deskripsi' => "Lumayan Lah"],
            ['id'=>4,'Nama' => "Romance",'deskripsi' => "Jelek"],
            ['id'=>5,'Nama' => "Action",'deskripsi' => "Kurang Baik"],
            ['id'=>6,'Nama' => "Adventure",'deskripsi' => "Bagus"],
            ['id'=>7,'Nama' => "Comedy",'deskripsi' => "Bagus Aja"],
            ['id'=>8,'Nama' => "Kriminal",'deskripsi' => "Lumayan Lah"],
            ['id'=>9,'Nama' => "Fantasi",'deskripsi' => "Kurang Menarik"],
            ['id'=>10,'Nama' => "Musikal",'deskripsi' => "Bagus Banget"],
            ['id'=>11,'Nama' => "Epik",'deskripsi' => "Luamayan Jelek"],
            ['id'=>12,'Nama' => "Fiksi Ilmiah",'deskripsi' => "Bagus"],
            ['id'=>13,'Nama' => "Jagal",'deskripsi' => "Bagus"],
            ['id'=>14,'Nama' => "Seru",'deskripsi' => "Jelek"],
            ['id'=>15,'Nama' => "Cerita",'deskripsi' => "Jelek"],
          ]);
    }
}
