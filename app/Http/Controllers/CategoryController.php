<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use App\Category;

class CategoryController extends Controller
{

    public function index(Request $request)
    {
      $categorys = Category::paginate(5);
      $categorys = Category::where([
          ['Nama', 'LIKE', '%' . $request->search . '%'],
        ])->paginate(5);
      return view('categorys.index',['categorys'=>$categorys]);
    }

    public function create()
    {
        return view('categorys.create');
    }

    public function store(Request $request)
    {
      $request->validate([
        'Nama'=>'required|min:5|max:100|unique:users',
        'deskripsi'=>'required|max:200',
      ]);
        $new_categorys = new Category;
        $new_categorys->Nama=$request->Nama;
        $new_categorys->deskripsi = $request->deskripsi;
        $new_categorys->save();
        return redirect()->route('categorys.create')->with('status','Category Succesfully Created');
    }

    public function show($id)
    {
      $category = Category::findOrFail($id);
      return view('categorys.show',['category'=>$category]);
    }

    public function edit($id)
    {
      $category = Category::findOrFail($id);
      return view('categorys.edit',['category'=>$category]);
    }

    public function update(Request $request, $id)
    {
      $update_category = Category::findOrFail($id);
      $update_category->Nama = $request->get('Nama');
      $update_category->deskripsi = $request->get('deskripsi');
      $update_category->save();
      return redirect()->route('categorys.edit',['id'=>$id])->with('status','Category succesfullyupdated');
    }

    public function destroy($id)
    {
      $category = Category::findOrFail($id);
      $category->delete();
      return redirect()->route('categorys.index')->with('status', 'Category Successfully deleted');
    }
}
