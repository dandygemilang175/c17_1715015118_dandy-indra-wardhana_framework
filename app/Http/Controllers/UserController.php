<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserController extends Controller
{
    public function index(Request $request)
    {
      $users = User::paginate(5);
      $users = User::where([
          ['Email', 'LIKE', '%' . $request->search . '%'],
        ])->paginate(5);
      return view('users.index',['users'=>$users]);
    }

    public function create()
    {
        return view('users.create');
    }

    public function store(Request $request)
    {
      $request->validate([
        'Nama'=>'required|min:5|max:100|unique:users',
        'Address'=>'required|max:200',
        'Phone'=>'required|min:10|max:12',
        'avatar'=>'required|mimes:jpg,jpeg,png,bmp',
        'Email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'password' => ['required', 'string', 'min:6', 'confirmed'],
      ]);
        $new_user = new User;
        $new_user->Nama=$request->Nama;
        $new_user->Email = $request->Email;
        $new_user->Address= $request->Address;
        $new_user->Phone= $request->Phone;
        $new_user->password = Hash::make($request->password);
        if ($request->file('avatar')) {
          $file = $request->file('avatar')->store('avatars','public');
          $new_user->avatar = $file;
          }
          $new_user->save();
        return redirect()->route('users.create')->with('status','User Succesfully Created');
    }

    public function show($id)
    {
      $user = User::findOrFail($id);
      return view('users.show',['user'=>$user]);
    }

    public function edit($id)
    {
      $user = User::findOrFail($id);
      return view('users.edit',['user'=>$user]);
    }

    public function update(Request $request, $id)
    {
      $request->validate([
        'Nama'=>'required|min:5|max:100',
        'Address'=>'required|max:200',
        'Phone'=>'required|min:10|max:12',
      ]);
       $update_user = User::findOrFail($id);
       $update_user->Nama = $request->Nama;
       $update_user->Address = $request->Address;
       $update_user->Phone = $request->Phone;
       if ($request->file('avatar')) {
         if ($update_user->avatar && file_exists(storage_path('app/public/'.$update_user->avatar))) {
              Storage::delete('public/'.$update_user->avatar);
         }
         $file = $request->file('avatar')->store('avatars','public');
         $update_user->avatar = $file;
       }
       $update_user->save();

      return redirect()->route('users.edit',['id'=>$id])->with('status','User Successfully Updated');

    }

    public function destroy($id)
    {
      $user = User::findOrFail($id);
      if ($user->avatar && file_exists(storage_path('app/public/'.$user->avatar))) {
        Storage::delete('public/'.$user->avatar);
          }
      $user->delete();
      return redirect()->route('users.index')->with('status', 'User Successfully deleted');
    }
}
