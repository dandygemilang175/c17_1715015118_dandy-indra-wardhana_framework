<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use App\Book;
use App\Category;


class BukuController extends Controller
{
    public function index(Request $request)
    {
      $books = Book::paginate(5);
      $books = Book::where([
          ['Judul', 'LIKE', '%' . $request->search . '%'],
        ])->paginate(5);
      return view('buku.index',['books'=>$books]);
    }

    public function create()
    {
      $categorys = Category::all();
        return view('buku.create',['categorys'=>$categorys]);
    }

    public function store(Request $request)
    {
      $request->validate([
        'Judul'=>'required|min:5|max:100',
        'deskripsi'=>'required',
        'Penerbit'=>'required|max:200',
        'avatar'=>'required|mimes:jpg,jpeg,png,bmp',
        'Penulis'=>'required|max:200',
        'Stok' =>  'required',
        'Harga' =>'required|max:200',
      ]);
        $new_book = new Book;
        $new_book->Judul=$request->Judul;
        $new_book->Penulis = $request->Penulis;
        $new_book->Penerbit = $request->Penerbit;
        $new_book->Harga = $request->Harga;
        $new_book->Stok =$request->Stok;
        $new_book->deskripsi =$request->deskripsi;
        if ($request->file('avatar')) {
          $file = $request->file('avatar')->store('avatars','public');
          $new_book->avatar = $file;
          }
          $new_book->save();
          $categorys= $request->categorys;
          $new_book->categorys()->attach($categorys);
        return redirect()->route('books.create')->with('status','Book Succesfully Created');
    }

    public function show($id)
    {
      $book = Book::findOrFail($id);
      return view('buku.show',['book'=>$book]);
    }

    public function edit($id)
    {
      $book = Book::findOrFail($id);
      $books=[
        'book' => Book::findOrFail($id),
        'book_categorys' => $book->categorys,
        'categorys' => Category::all(),
      ];

      return view('buku.edit')->with($books);
    }

    public function update(Request $request, $id)
    {
      $update_book = Book::findOrFail($id);
      $update_book->Judul = $request->get('Judul');
      $update_book->Penulis = $request->get('Penulis');
      $update_book->Penerbit = $request->get('Penerbit');
      $update_book->Harga = $request->get('Harga');
      $update_book->deskripsi = $request->get('deskripsi');
      $update_book->Stok = $request->get('Stok');
        if ($request->file('avatar')) {
            if ($update_book->avatar &&
              file_exists(storage_path('app/public/'.$update_book->avatar))) {
                Storage::delete('public/'.$update_book->avatar);
                }
      $file = $request->file('avatar')->store('avatars','public');
      $update_book->avatar = $file;
      }

      $update_book->save();
      return redirect()->route('books.edit',['id'=>$id])->with('status','Book succesfullyupdated');
    }

    public function destroy($id)
    {
      $book = Book::findOrFail($id);
      if ($book->avatar && file_exists(storage_path('app/public/'.$book->avatar))) {
        Storage::delete('public/'.$book->avatar);
          }
      $book->delete();
      return redirect()->route('books.index')->with('status', 'Book Successfully deleted');
    }

}
