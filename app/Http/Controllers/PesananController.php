<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use App\Order;
use App\User;

class PesananController extends Controller
{
    public function index(Request $request)
    {
      $orders = Order::paginate(5);
      $orders= Order::where([
          ['Nomor', 'LIKE', '%' . $request->search . '%'],
        ])->paginate(5);
      return view('pesanan.index',['orders'=>$orders]);
    }

    public function create()
    {
        $users=User::all();
        return view('pesanan.create',['users'=>$users]);
    }

    public function store(Request $request)
    {
      $request->validate([
        'Nomor'=>'required|min:5|max:100|unique:orders',
        'Total'=>'required|max:100',
        'status'=>'required|max:100',
        'user_id'=>'required',
      ]);

        $new_order = new Order;
        $new_order->Nomor=$request->Nomor;
        $new_order->Total = $request->Total;
        $new_order->user_id= $request->user_id;
        $new_order->status= $request->status;
        $new_order->save();
        return redirect()->route('orders.create')->with('status','Order Successfully Added');

    }

    public function show($id)
    {
      $order = Order::findOrFail($id);
      return view('pesanan.show',['order'=>$order]);
    }

    public function edit($id)
    {
      $order = Order::findOrFail($id);
      $users = User::all();
      return view('pesanan.edit',['order'=>$order, 'users'=>$users]);
    }

    public function update(Request $request, $id)
    {
      $update_order = Order::findOrFail($id);
      $update_order->status = $request->get('status');
      $update_order->Total = $request->get('Total');
      $update_order->save();
      return redirect()->route('orders.edit',['id'=>$id])->with('status','Order succesfullyupdated');
    }

    public function destroy($id)
    {
          $order = Order::findOrFail($id);
          $order->delete();
          return redirect()->route('orders.index')->with('status', 'Order Successfully deleted');
    }

}
