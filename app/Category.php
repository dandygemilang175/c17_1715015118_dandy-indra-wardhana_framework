<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Book;

class Category extends Model
{

    protected $fillable = [
      'id',
      'Nama',
      'deskripsi',
    ];

    public function books(){
      return $this->belongsToMany(Book::class);
    }
}
