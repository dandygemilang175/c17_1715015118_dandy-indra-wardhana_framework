<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class Book extends Model
{

  protected $fillable=[
    'id',
    'Judul',
    'Penulis',
    'Penerbit',
    'Harga',
    'deskripsi',
    'Stok',
  ];

  public function categorys(){
    return $this->belongsToMany(Category::class);
  }
}
