@extends('templates.home')
@section('title')
 Create Book
@endsection
@section('content')
  <div class="container" >
    <h3>Create Book</h3>
    <hr>
    @if (session('status'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{ session('status') }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    @endif
      <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
        <div class="card-header bg-primary text-white">
          <h5> Create a New Book</h5>
        </div>
          <div class="card-body">
            <div class="container text-primary">
              <form action="{{ route('books.store') }}" class="form-group" method="POST" enctype="multipart/form-data">
                @csrf
                  <div class="row" >
                    <div class="col-md-3">
                      <label for="Judul" >Judul</label>
                    </div>
                    <div class="col-md-8">
                      <input value="{{ old('Judul') }}" type="text" class="form-control {{$errors->first('Judul') ? "is-invalid": ""}}" name="Judul" id="Judul">
                        <div class="invalid-feedback">
                           {{$errors->first('Judul')}}
                        </div>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-md-3">
                      <label for="Penulis">Penulis</label>
                    </div>
                    <div class="col-md-8">
                      <input value="{{ old('Penulis') }}" type="Penulis" class="form-control {{$errors->first('Penulis') ? "is-invalid": ""}}" name="Penulis" id="Penulis">
                        <div class="invalid-feedback">
                           {{$errors->first('Penulis')}}
                        </div>
                    </div>
                  </div>
                  <br>
                  <div class="row" >
                    <div class="col-md-3">
                      <label for="Penerbit">Penerbit</label>
                    </div>
                    <div class="col-md-8">
                      <input value="{{ old('Penerbit') }}" type="Penerbit" class="form-control {{$errors->first('Penerbit') ? "is-invalid": ""}}" name="Penerbit" id="Penerbit">
                        <div class="invalid-feedback">
                           {{$errors->first('Penerbit')}}
                        </div>
                    </div>
                  </div>
                  <br>
                  <div class="row" >
                    <div class="col-md-3">
                      <label for="Harga">Harga</label>
                    </div>
                    <div class="col-md-8">
                      <input value="{{ old('Harga') }}" type="Harga" class="form-control {{$errors->first('Harga') ? "is-invalid": ""}}" name="Harga" id="Harga">
                        <div class="invalid-feedback">
                           {{$errors->first('Harga')}}
                        </div>
                    </div>
                  </div>
                  <br>
                  <div class="row" >
                    <div class="col-md-3">
                      <label for="deskripsi">Deskripsi</label>
                    </div>
                    <div class="col-md-8">
                      <textarea name="deskripsi" class="form-control {{$errors->first('deskripsi') ? "is-invalid": ""}}" id="deskripsi" cols="20" rows="5">{{ old('deskripsi')}}</textarea>
                        <div class="invalid-feedback">
                           {{$errors->first('deskripsi')}}
                        </div>
                    </div>
                  </div>
                  <br>
                  <div class="row" >
                    <div class="col-md-3">
                      <label for="Stok" >Stok</label>
                    </div>
                    <div class="col-md-8">
                      <input value="{{ old('Stok') }}" type="number" class="form-control {{$errors->first('Stok') ? "is-invalid": ""}}" name="Stok" id="Stok">
                        <div class="invalid-feedback">
                           {{$errors->first('Stok')}}
                        </div>
                    </div>
                  </div>
                  <br>
                   <div class="row" >
                     <div class="col-md-3">
                       <div class="form-group">
                     <label for="Genre">Category</label>
                      </div>
                    </div>
                    <div class="col-md-8">
                      <select multiple class="form-control" id="category" name="category[]">
                     @foreach ($categorys as $category)
                         <option value='{{  $category['id'] }}'>{{ $category['Nama'] }}</option>
                     @endforeach
                      </select>
                      </div>
                    </div>
                  <br>
                  <div class="row">
                    <div class="input-group mb-3">
                      <div class="col-md-3 text-primary">
                        Avatar
                      </div>
                      <div class="col-md-8">
                        <div class="custom-file">
                          <label for="avatar" class="custom-file-label">avatar</label>
                          <input type="file" class="custom-file-input {{$errors->first('avatar') ? "is-invalid": ""}}" name="avatar" id="avatar">
                          <div class="invalid-feedback">
                            {{$errors->first('avatar')}}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-md-3 offset-md-5 offset-sm-4">
                      <button type="submit" class="btn btn-outline-primary">Create</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
@endsection
