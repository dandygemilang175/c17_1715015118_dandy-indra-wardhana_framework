@extends('templates.home')
@section('title')
Detail Book
@endsection
@section('content')
<h1>Detail Book </h1>
<hr>
@if (session('status'))
  <div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ session('status') }}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif
<br>
<div class="card bg-white border-info" style="max-width:70%; margin:auto; min-height:400px;">
    <div class="row " style="padding:25px">
        <div class="col-md-2 offset-md-5 offset-sm-4">
            <img src="{{asset('Storage/'.$book['avatar'])}}" style="height:150px; width:150px;" class="rounded-circle" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <h3>{{ $book['Judul'] }}</h3>
        </div>
    </div>
    <hr>
    <br>
    <div class="row">
        <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
            Penulis
        </div>
        <div class="col-md-4 col-sm-4">
            {{ $book['Penulis'] }}
        </div>
        <br>
        <br>
    </div>
    <div class="row">
        <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
            Penerbit
        </div>
        <div class="col-md-4 col-sm-4">
            {{ $book['Penerbit'] }}
        </div>
        <br>
        <br>
    </div>
    <div class="row">
      <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
        Category
      </div>
      <div class="col-md-4 col-sm-4">
        @foreach ($book->categorys as $category)
            {{ $category->Nama }}
        @endforeach
      </div>
      <br>
      <br>
    </div>
    <div class="row">
        <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
            Harga
        </div>
        <div class="col-md-4 col-sm-4">
            {{ $book['Harga'] }}
        </div>
        <br>
        <br>
    </div>
    <div class="row">
        <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
            Stok
        </div>
        <div class="col-md-4 col-sm-4">
            {{ $book['Stok'] }}
        </div>
        <br>
        <br>
    </div>
    <div class="row">
        <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
            Deskripsi
        </div>
        <div class="col-md-4 col-sm-4 ">
            {{ $book['deskripsi'] }}
        </div>
     </div>
     <br>
@endsection
