@extends('templates.home')
@section('title')
Daftar Book
@endsection
@section('css')
<style>
body{
    padding-top: 30px;
    }
    th, td {
        padding: 10px;
        text-align: center;
        }
        td a{
            margin: 3px;
            align-content: center;
            color: white;
             }
             td a:hover{
                 text-decoration: none;
                 }
                 td button{
                     margin-top: 5px;
                     cursor: pointer;
                    }
                    </style>
                    @endsection
                    @section('content')
                    <div class="container">
                        <h3> Daftar Book</h3><hr>
                        <div class="row">
                            <div class="col-md-2">
                                <a class="btn btn-outline-primary " href="{{ route('books.create') }}">
                                    <span data-feather="plus-circle"></span>
                                    Tambah<span class="sr-only">(current)</span>
                                </a>
                            </div>
                            <div class="col-md-10 ">
           <form class="" method="get">
             <div class="row">
               <div class="col-sm-6">
                 <input type="text" class="form-control" name="search" placeholder="Filter by Title">
               </div>
               <div class="col-sm-2">
                 <button type="submit" class="btn btn-primary">Filter</button>
               </div>
             </div>
           </form>
         </div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr class="table-primary">
                                        <th scope="col">id</th>
                                        <th scope="col">Judul</th>
                                        <th scope="col">Picture</th>
                                        <th scope="col">action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($books as $book)
                                    <tr>
                                        <td>{{ $book['id'] }}</td>
                                        <td>{{ $book['Judul'] }}</td>
                                        <td> <img src="{{ asset('Storage/'.$book['avatar']) }}" alt="" width="80px"></td>
                                        <td>
                                            <a class="btn-sm btn-primary" href="{{ route('books.show',$book['id']) }}">
                                                <span data-feather="eye"></span>
                                                Detail <span class="sr-only">(current)</span></a>
                                                <a class="btn-sm btn-success d-inline" href="{{ route('books.edit',$book['id']) }}">
                                                    <span data-feather="edit-2"></span>
                                                    Edit <span class="sr-only">(current)</span></a>
                                                    <form class="d-inline"
                                                    onsubmit="return confirm('Delete this Book permanently?')"
                                                    action="{{route('books.destroy', $book['id'])}}"
                                                    method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn-sm btn-danger" value="Delete">
                                                        <span data-feather="trash"></span>
                                                        Delete <span class="sr-only">(current)</span></button>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {{$books->links()}}
                                </div>
                             </div>
                              @endsection
