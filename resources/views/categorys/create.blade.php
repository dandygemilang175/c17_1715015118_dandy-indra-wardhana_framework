@extends('templates.home')
@section('title')
 Create Category
@endsection
@section('content')
  <div class="container" >
    <h3>Create User</h3>
    <hr>
    @if (session('status'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{ session('status') }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    @endif
      <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
        <div class="card-header bg-primary text-white">
          <h5> Create a New Category</h5>
        </div>
          <div class="card-body">
            <div class="container text-primary">
              <form action="{{ route('categorys.store') }}" class="form-group" method="POST" enctype="multipart/form-data">
                @csrf
                  <div class="row" >
                    <div class="col-md-3">
                      <label for="Nama" >Username</label>
                    </div>
                    <div class="col-md-8">
                      <input value="{{ old('Nama') }}" type="text" class="form-control {{$errors->first('Nama') ? "is-invalid": ""}}" name="Nama" id="Nama">
                        <div class="invalid-feedback">
                           {{$errors->first('Nama')}}
                        </div>
                    </div>
                  </div>
                  <br>
                  <div class="row" >
                    <div class="col-md-3">
                      <label for="deskripsi">Deskripsi</label>
                    </div>
                    <div class="col-md-8">
                      <textarea name="deskripsi" class="form-control {{$errors->first('deskripsi') ? "is-invalid": ""}}" id="deskripsi" cols="20" rows="5">{{ old('deskripsi')}}</textarea>
                        <div class="invalid-feedback">
                           {{$errors->first('deskripsi')}}
                        </div>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-md-3 offset-md-5 offset-sm-4">
                      <button type="submit" class="btn btn-outline-primary">Create</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
@endsection
