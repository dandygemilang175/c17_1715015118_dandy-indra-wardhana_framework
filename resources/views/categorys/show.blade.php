@extends('templates.home')
@section('title')
Detail Category
@endsection
@section('content')
<h1>Detail Category </h1>
<hr>
<br>
<div class="card bg-white border-info" style="max-width:70%; margin:auto; min-height:400px;">
    <div class="row">
        <div class="col-md-12 text-center">
            <h3>{{ $category['Nama'] }}</h3>
        </div>
    </div>
    <hr>
    <br>
    <div class="row">
        <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
            Deskripsi
        </div>
        <div class="col-md-4 col-sm-4">
            {{ $category['deskripsi'] }}             
        </div>
        <br>
    </div>
@endsection
