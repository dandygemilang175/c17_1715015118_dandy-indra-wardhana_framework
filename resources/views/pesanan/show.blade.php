@extends('templates.home')
@section('title')
Detail Order
@endsection
@section('content')
<h1>Detail Order </h1>
<hr>
<br>
<div class="card bg-white border-info" style="max-width:70%; margin:auto; min-height:400px;">
    <div class="row " style="padding:25px">
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <h3>{{ $order['Nomor'] }}</h3>
        </div>
    </div>
    <hr>
    <br>
    <div class="row">
        <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
            Total
        </div>
        <div class="col-md-4 col-sm-4">
            {{ $order['Total'] }}
        </div>
        <br>
        <br>
    </div>
    <br>
    <div class="row">
        <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
            User
        </div>
        <div class="col-md-4 col-sm-4">
            {{ $order->users->Nama }}
        </div>
        <br>
        <br>
    </div>
    <div class="row">
        <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
            Status
        </div>
        <div class="col-md-4 col-sm-4 ">
            {{ $order['status'] }}
        </div>
     </div>
     <br>
@endsection
