@extends('templates.home')
@section('title')
Edit Order
@endsection
@section('content')
<div class="container" >
    <h3>Form Edit Order</h3>
    <hr>
    @if (session('status'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{ session('status') }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    @endif
    <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
        <div class="card-header bg-primary text-white">
            <h5>{{ $order['Nomor'] }}</h5>
            </div>
            <div class="card-body">
                <div class="container text-primary">
                    <form action="{{ route('orders.update',$order['id']) }}" method="POST" class="formgroup" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                <br>
                <div class="row">
                    <div class="col-md-3">
                        <label for="Total" class="text-primary">Total</label>
                    </div>
                    <div class="col-md-8">
                        <input type="number" class="form-control" name="Total" id="Total" value="{{ $order['Total'] }}">
                    </div>
                </div>
                <br>
                <div class="row">
                <div class="col-md-3">
                  <label for="user_id">User</label>
                </div>
                <div class="col-md-8">
                  <select name="user_id" id="user_id" class="form-control
                  {{$errors->first('user_id') ? "is-invalid": ""}}">
                    <option value=""></option>
                  @foreach ($users as $user)
                    <option value="{{ $user->id }}" @if ($user->id == $order->users->id) selected @endif>{{ $user->Nama }}</option>
                  @endforeach
                </select>
                <div class="invalid-feedback">
                  {{$errors->first('user_id')}}
                </div>
              </div>
              </div>
                <br>
                <div class="row">
                    <div class="col-md-3">
                        <label for="status" class="text-primary">Status</label>
                    </div>
                    <div class="col-md-8">
                       @if ($order['status']=="Finish")
                       <select class="form-control"  name='status'>
                            <option value="Finish" selected>Finish</option>
                            <option value="Process">Process</option>
                            <option value="Cancel">Cancel</option>
                             <option value="Submit">Submit</option>
                        </select>
                       @elseif($order['status']=="Process")
                       <select class="form-control" name='status' >
                            <option value="Finish" >Finish</option>
                            <option value="Process" selected>Process</option>
                            <option value="Cancel">Cancel</option>
                             <option value="Submit">Submit</option>
                        </select>
                        @elseif($order['status']=="Cancel")
                        <select class="form-control" name='status' >
                             <option value="Finish" >Finish</option>
                             <option value="Process">Process</option>
                             <option value="Cancel" selected>Cancel</option>
                             <option value="Submit">Submit</option>
                         </select>
                         @elseif($order['status']=="Submit")
                         <select class="form-control"  name='status'>
                              <option value="Finish" >Finish</option>
                              <option value="Process">Process</option>
                              <option value="Cancel">Cancel</option>
                              <option value="Submit" selected>Submit</option>
                          </select>

                       @endif

                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-3 offset-md-5 offset-sm-4">
                        <button type="submit" class="btn btn-outline-primary" >Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
@endsection
