@extends('templates.home')
@section('title')
Daftar Order
@endsection
@section('css')
<style>
body{
    padding-top: 30px;
    }
    th, td {
        padding: 10px;
        text-align: center;
        }
        td a{
            margin: 3px;
            align-content: center;
            color: white;
             }
             td a:hover{
                 text-decoration: none;
                 }
                 td button{
                     margin-top: 5px;
                     cursor: pointer;
                    }
                    </style>
                    @endsection
                    @section('content')
                    <div class="container">
                        <h3> Daftar Order</h3><hr>
                        <div class="row">
                            <div class="col-md-2">
                                <a class="btn btn-outline-primary " href="{{ route('orders.create') }}">
                                    <span data-feather="plus-circle"></span>
                                    Tambah<span class="sr-only">(current)</span>
                                </a>
                          </div>
                            <div class="col-md-10 ">
                                <form class="form-inline" method="get">
                                    <div class="row">
                                        <div class="col-sm-6">
                                          <input type="text" class="form-control" name="search" placeholder="Filter by Invoice Number">
                                        </div>
                                        <div class="col-sm-4">
                                          <select class="form-control {{ $errors->first('jumlah')?'is-invalid':'' }}" name="status" id="status">
                                              <option value="Any">Any</option>
                                              <option value="Success">Success</option>
                                              <option value="Process">Process</option>
                                              <option value="Cancel">Cancel</option>
                                            </select>
                                          </div>
                                        <div class="col-sm-2">
                                          <button type="submit" class="btn btn-primary">Filter</button>
                                        </div>
                                      </div>
                                    </form>
                                  </div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr class="table-primary">
                                        <th scope="col">id</th>
                                        <th scope="col">Nomor</th>
                                        <th scope="col">Total</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($orders as $order)
                                    <tr>
                                        <td>{{ $order['id'] }}</td>
                                        <td>{{ $order['Nomor'] }}</td>

                                        <td>{{ $order['Total'] }}</td>
                                        <td>
                                             @if ($order['status']=='Finish')
                                                 <div class="badge badge-success">  {{$order['status']}}</div>
                                             @elseif ($order['status']=='Process')
                                             <div class="badge badge-warning">  {{$order['status']}}</div>
                                             @elseif ($order['status']=='Cancel')
                                             <div class="badge badge-danger">  {{$order['status']}}</div>
                                             @elseif ($order['status']=='Submit')
                                             <div class="badge badge-success">  {{$order['status']}}</div>
                                             @endif
                                        </td>
                                        <td>
                                            <a class="btn-sm btn-primary" href="{{ route('orders.show',$order['id']) }}">
                                                <span data-feather="eye"></span>
                                                Detail <span class="sr-only">(current)</span></a>
                                                <a class="btn-sm btn-success d-inline" href="{{ route('orders.edit',$order['id']) }}">
                                                    <span data-feather="edit-2"></span>
                                                    Edit <span class="sr-only">(current)</span></a>
                                                    <form class="d-inline"
                                                    onsubmit="return confirm('Delete this Order permanently?')"
                                                    action="{{route('orders.destroy', $order['id'])}}"
                                                    method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn-sm btn-danger" value="Delete">
                                                        <span data-feather="trash"></span>
                                                        Delete <span class="sr-only">(current)</span></button>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {{$orders->links()}}
                                </div>
                             </div>
                              @endsection
