@extends('templates.home')
@section('title')
Create Order
@endsection

@section('content')
<div class="container" >
    <h3>Create Order</h3>
    <hr>
    @if (session('status'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{ session('status') }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    @endif
    <div class="card  border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
        <div class="card-header bg-primary text-white">
            <h5> Create a New Order</h5>
        </div>
        <div class="card-body">
            <div class="container text-primary">
                <form action="{{ route('orders.store') }}" class="form-group" method="POST" enctype="multipart/form-data">
                  @csrf
                    <div class="row" >
                      <div class="col-md-3">
                        <label for="Nomor" >Nomor Invoice</label>
                      </div>
                      <div class="col-md-8">
                        <input value="{{old('Nomor')}}" type="text" class="form-control {{ $errors->first('Nomor')?'is-invalid':'' }}" name="Nomor" id="Nomor">
                        <div class="invalid-feedback">
                           {{$errors->first('Nomor')}}
                        </div>
                      </div>
                    </div>
                    <br>
                    <div class="row" >
                      <div class="col-md-3">
                        <label for="Total">Total</label>
                      </div>
                      <div class="col-md-8">
                        <input value="{{old('Total')}}" type="text" class="form-control {{ $errors->first('Total')?'is-invalid':'' }}" name="Total" id="Total">
                        <div class="invalid-feedback">
                           {{$errors->first('Total')}}
                        </div>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                    <div class="col-md-3">
                      <label for="user_id">User</label>
                    </div>
                    <div class="col-md-8">
                      <select name="user_id" id="user_id" class="form-control
                      {{$errors->first('user_id') ? "is-invalid": ""}}">
                        <option value=""></option>
                      @foreach ($users as $user)
                        <option value="{{ $user->id }}">{{ $user->Nama }}</option>
                      @endforeach
                    </select>
                    <div class="invalid-feedback">
                      {{$errors->first('user_id')}}
                    </div>
                  </div>
                </div>
                    <br>
                      <div class="row">
                        <div class="col-md-3">
                            <label for="Status">Status</label>
                        </div>
                        <div class="col-md-8">
                            <select class="form-control"  name="status">
                                <option value="Finish">Finish</option>
                                <option value="Process">Process</option>
                                <option value="Cancel">Cancel</option>
                                <option value="Submit">Submit</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3 offset-md-5 offset-sm-4">
                            <button type="submit" class="btn btn-outline-primary">Create</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
