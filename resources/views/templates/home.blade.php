<!doctype html> 
<html lang="en">     
    @include('templates.partials._head')
    <body>         
        @include('templates.partials._topnav')       
        <div class="container-fluid">         
            <div class="row">             
                @include('templates.partials._sidenav')
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">             
                    @yield('content')             
                </main>         
            </div>         
        </div>          
        <footer class="footer">             
            <div class="container text-center text-light">             
                <span>&copy;Dandy Indra Bahhh</span>             
            </div>         
        </footer>         
        <!-- Placed at the end of the document so the pages load faster -->         
        @include('templates.partials._script') 
        </body> 
        </html 