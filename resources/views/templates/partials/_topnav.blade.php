<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand" href="#" style="padding-left:5px">Dandy Indra W Shop</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" arialabel="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="{{ route('users.index') }}">Users</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('books.index') }}">Books</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="{{ route('categorys.index') }}">Categories</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('orders.index') }}">Orders</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="text" placeholder="Search" arialabel="Search">
      <button class="btn btn-outline-success my-4 my-sm-2" type="submit"> <span data-feather="search"></span></button>
    </form>
    <ul class="navbar-nav px-4">



      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Username</a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
          <a class="dropdown-item" href="#">Logout</a>
        </div>

      </li>
    </ul>
  </div>
</nav>
