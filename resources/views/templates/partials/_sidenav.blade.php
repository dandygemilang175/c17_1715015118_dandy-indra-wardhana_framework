<nav class="col-md-2 d-none d-md-block bg-light sidebar">
  <div class="sidebar-sticky">
    <ul class="nav flex-column">
      <li class="nav-item ">
        <a class="nav-link active" href="{{ route('users.index') }}">
          <span data-feather="user"></span> Users </a>
      </li>
      <li class="nav-item ">
        <a class="nav-link active" href="{{ route('books.index') }}">
          <span data-feather="book-open"></span> Books </a>
      </li>
      <li class="nav-item ">
        <a class="nav-link active" href="{{ route('categorys.index') }}">
          <span data-feather="align-center"></span> Categories </a>
      </li>
      <li class="nav-item ">
        <a class="nav-link active" href="{{ route('orders.index') }}">
          <span data-feather="shopping-cart"></span> Order </a>
      </li>

    </ul>
  </div>
</nav>
