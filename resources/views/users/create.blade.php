@extends('templates.home')
@section('title')
 Create User
@endsection
@section('content')
  <div class="container" >
    <h3>Create User</h3>
    <hr>
    @if (session('status'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{ session('status') }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    @endif
      <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
        <div class="card-header bg-primary text-white">
          <h5> Create a New User</h5>
        </div>
          <div class="card-body">
            <div class="container text-primary">
              <form action="{{ route('users.store') }}" class="form-group" method="POST" enctype="multipart/form-data">
                @csrf
                  <div class="row" >
                    <div class="col-md-3">
                      <label for="Nama" >Username</label>
                    </div>
                    <div class="col-md-8">
                      <input value="{{ old('Nama') }}" type="text" class="form-control {{$errors->first('Nama') ? "is-invalid": ""}}" name="Nama" id="Nama">
                        <div class="invalid-feedback">
                           {{$errors->first('Nama')}}
                        </div>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-md-3">
                      <label for="Email">Email</label>
                    </div>
                    <div class="col-md-8">
                      <input value="{{ old('Email') }}" type="Email" class="form-control {{$errors->first('Email') ? "is-invalid": ""}}" name="Email" id="Email">
                        <div class="invalid-feedback">
                           {{$errors->first('Email')}}
                        </div>
                    </div>
                  </div>
                  <br>
                  <div class="row" >
                    <div class="col-md-3">
                      <label for="password">Password</label>
                    </div>
                    <div class="col-md-8">
                      <input value="{{ old('password') }}" type="password" class="form-control {{$errors->first('password') ? "is-invalid": ""}}" name="password" id="password">
                        <div class="invalid-feedback">
                           {{$errors->first('password')}}
                        </div>
                    </div>
                  </div>
                  <br>
                  <div class="row" >
                    <div class="col-md-3">
                      <label for="password_confirmation">Password Confirmation</label>
                    </div>
                    <div class="col-md-8">
                      <input value="{{ old('password') }}" type="password" class="form-control {{$errors->first('password_confirmation') ? "is-invalid": ""}}" name="password_confirmation" id="password_confirmation">
                        <div class="invalid-feedback">
                           {{$errors->first('password_confirmation')}}
                        </div>
                    </div>
                  </div>
                  <br>
                  <div class="row" >
                    <div class="col-md-3">
                      <label for="Address">Address</label>
                    </div>
                    <div class="col-md-8">
                      <textarea name="Address" class="form-control {{$errors->first('Address') ? "is-invalid": ""}}" id="Address" cols="20" rows="5">{{ old('Address')}}</textarea>
                        <div class="invalid-feedback">
                           {{$errors->first('Address')}}
                        </div>
                    </div>
                  </div>
                  <br>
                  <div class="row" >
                    <div class="col-md-3">
                      <label for="Phone" >Phone</label>
                    </div>
                    <div class="col-md-8">
                      <input value="{{ old('Phone') }}" type="number" class="form-control {{$errors->first('Phone') ? "is-invalid": ""}}" name="Phone" id="Phone">
                        <div class="invalid-feedback">
                           {{$errors->first('Phone')}}
                        </div>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="input-group mb-3">
                      <div class="col-md-3 text-primary">
                        Avatar
                      </div>
                      <div class="col-md-8">
                        <div class="custom-file">
                          <label for="avatar" class="custom-file-label">avatar</label>
                          <input type="file" class="custom-file-input {{$errors->first('avatar') ? "is-invalid": ""}}" name="avatar" id="avatar">
                          <div class="invalid-feedback">
                            {{$errors->first('avatar')}}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-md-3 offset-md-5 offset-sm-4">
                      <button type="submit" class="btn btn-outline-primary">Create</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
@endsection
