@extends('templates.home')
@section('title')
 Edit User
@endsection
@section('content')
<div class="container" >
    <h3>Form Edit User</h3>
    <hr>
    @if (session('status'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{ session('status') }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    @endif
    <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
      <div class="card-header bg-primary text-white">
        <h5>{{ $user['Nama'] }}</h5>
      </div>
      <div class="card-body">
        <div class="container text-primary">
        </form>
          <form action="{{ route('users.update',$user['id']) }}" method="POST" class="formgroup" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row" >
              <div class="col-md-3">
                <label for="Nama" >Username</label>
              </div>
              <div class="col-md-8">
                <input value="{{ $user['Nama'] }}" type="text" class="form-control {{$errors->first('Nama') ? "is-invalid": ""}}" name="Nama" id="Nama">
                  <div class="invalid-feedback">
                     {{$errors->first('Nama')}}
                  </div>
              </div>
            </div>
            <br>
            <div class="row" >
              <div class="col-md-3">
                <label for="Address">Address</label>
              </div>
              <div class="col-md-8">
                <textarea name="Address" class="form-control {{$errors->first('Address') ? "is-invalid": ""}}" id="Address" cols="20" rows="5">{{ $user['Address']}}</textarea>
                  <div class="invalid-feedback">
                     {{$errors->first('Address')}}
                  </div>
              </div>
            </div>
            <br>
            <div class="row" >
              <div class="col-md-3">
                <label for="Phone" >Phone</label>
              </div>
              <div class="col-md-8">
                <input value="{{ $user['Phone'] }}" type="number" class="form-control {{$errors->first('Phone') ? "is-invalid": ""}}" name="Phone" id="Phone">
                  <div class="invalid-feedback">
                     {{$errors->first('Phone')}}
                  </div>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="input-group mb-3">
                <div class="col-md-3 text-primary">
                  Avatar
                </div>
                <div class="col-md-8">
                  <img src="{{asset('storage/'.$user['avatar'])}}" class="img-thumbnail" height="150px" width="150px" alt="">
                  <div class="custom-file">
                    <label for="avatar" class="custom-file-label">avatar</label>
                    <input type="file" class="custom-file-input" name="avatar" id="avatar" value="{{asset('storage/'.$user['avatar'])}}" >
                  </div>
                </div>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-3 offset-md-5 offset-sm-4">
                <button type="submit" class="btn btn-outline-primary" >Update</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
 @endsection
