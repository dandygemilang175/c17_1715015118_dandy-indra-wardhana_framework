@extends('templates.home')
@section('title')
Detail User
@endsection
@section('content')
<h1>Detail User </h1>
<hr>
<br>
<div class="card bg-white border-info" style="max-width:70%; margin:auto; min-height:400px;">
    <div class="row " style="padding:25px">
        <div class="col-md-2 offset-md-5 offset-sm-4">
            <img src="{{asset('Storage/'.$user['avatar'])}}" style="height:150px; width:150px;" class="rounded-circle" alt="">             
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <h3>{{ $user['Nama'] }}</h3>
        </div>
    </div>
    <hr>
    <br>
    <div class="row">
        <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
            Email
        </div>
        <div class="col-md-4 col-sm-4">
            {{ $user['Email'] }}
        </div>
        <br>
    </div>
    <div class="row">
        <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
            Phone
        </div>
        <div class="col-md-4 col-sm-4">
            {{ $user['Phone'] }}
        </div>
        <br>
    </div>
    <div class="row">
        <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
            Address
        </div>
        <div class="col-md-4 col-sm-4 ">
            {{ $user['Address'] }}
        </div>
     </div>
@endsection
